﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

    public float cameraSpeed;
    public float zoomSpeed;

    private float horizInput;
    private float vertInput;
    private float wheelInput;

	// Use this for initialization
	void Start () {
        cameraSpeed = 0.5f;
        zoomSpeed = 75.0f;
	}
	
	// Update is called once per frame
	void Update () {
        
        horizInput = Input.GetAxis("Horizontal");
        vertInput = Input.GetAxis("Vertical");
        wheelInput = Input.GetAxis("Mouse ScrollWheel");

        this.transform.Translate(Vector3.right * cameraSpeed * horizInput, Space.World);
        this.transform.Translate(Vector3.forward * cameraSpeed * vertInput, Space.World);
        this.transform.Translate(Vector3.forward * zoomSpeed * wheelInput, Space.Self);

    }
}
