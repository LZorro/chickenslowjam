﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenController : MonoBehaviour {

    public float flightPower;

	// Use this for initialization
	void Start () {
        flightPower = 500.0f;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseDown()
    {
        Debug.Log("CLUCK!");
        this.gameObject.GetComponent<Rigidbody>().AddForce(this.transform.up * flightPower);
    }
}
